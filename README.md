# An improved hook script for DNS-01 and the GleSYS API.

Official script/repo can be found on [GleSYS](https://github.com/glesys/API/blob/master/BASH/LetsencryptGlesysHook/glesys-dns-01-hook.sh)
Github.

The first thing about that is that it does not work if you are using a more
recent version of dehydrated. 

The other thing is that it poses a security vulnerability when it puts your
access token on the command line. That means it can be read by a sneaky 
user or task, using the command ps for instance.

## Dependencies
- [dehydrated](https://github.com/lukas2511/dehydrated)
- curl
- xmlstarlet (debian: apt-get install curl xmlstarlet)
- GleSYS API credentials (DOMAIN permissions for list, add, remove records)

## Short tutorial
An outdated tutorial can be found [here](https://glesys.se/kb/api/letsencrypt-with-glesys-api).

Follow these simple instructions and you will be fine:

- Create an API key for the GleSYS API in the control panel
- The token needs to be able to list, add and remove records (but not domains)
- echo "export USER=CL12345" > /etc/ssl/private/glesys-credentials
- echo "export KEY=KEY_GOES_HERE" >> /etc/ssl/private/glesys-credentials
- chmod 600 /etc/ssl/private/glesys-credentials
- edit your /etc/dehydrated/config to include this hook script

## IMPORTANT
The Dehydrate config should have CHAIN="yes" in order for wildcard certs
to work in all cases. This is especially true if you want to create a combo 
cert for domain.tld and *.domain.tld, which I believe you do.

This script works only with the DNS-01 verification method.
